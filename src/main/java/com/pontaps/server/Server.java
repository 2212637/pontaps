package com.pontaps.server;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import java.util.Properties;

public class Server {

    public static void main(String[] args) {

        try {

            Properties props = new Properties();
            props.put("org.omg.CORBA.ORBInitialHost", "192.168.100.50");
            props.put("org.omg.CORBA.ORBInitialPort", "9090");

            // create and initialize the ORB
            ORB orb = ORB.init(args, props);

            // get reference to rootPOA and
            // activate the POAManager
            POA rootPOA = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
            rootPOA.the_POAManager().activate();

            // create servant and register it with the ORB
            PontapsServerImpl pontapsImpl = new PontapsServerImpl();

            // get object reference from the servant
            org.omg.CORBA.Object ref = rootPOA.servant_to_reference(pontapsImpl);
            Pontaps.Server href = Pontaps.ServerHelper.narrow(ref);

            // get the root naming context
            org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");

            // use NamingContextExt which is part of the Interoperable Naming Service (INS) specification.
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

            // bind the Object Reference in Naming
            String name = "Pontaps";
            NameComponent[] path = ncRef.to_name(name);
            ncRef.rebind(path, href);

            System.out.println("Pontaps server ready and waiting...");

            // wait for invocations from clients
            orb.run();

        } catch (Exception e) {
            e.printStackTrace();
        }// try-catch ends
        System.out.println("Terminating Pontaps...");

    }// method main ends
}
